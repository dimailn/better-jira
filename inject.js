$(function()
{
  var config = { attributes: true, childList: true, characterData: true, subtree: true }
  var observer = new MutationObserver(function(ms)
  {
    observer.disconnect();
    $('.external-link').attr('target', '_blank');

    $('.user-content-block li').each(function(i, li){
      el = $(li)
      html = el.html();
      if(html[0] == '+')
      {
        el.html('<del>' + html.substr(1, html.length - 1) + '</del>');

      }
    });
    observer.observe(document.body, config);
  });
  observer.observe(document.body, config);
});